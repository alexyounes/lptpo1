package Utiles;

import static Utiles.UtilesImprimir.imprimirLineaColor;

/**
 *
 * @author Alejandro Younes
 */
public class UtilesHilo {

    public static String nombre() {
        return Thread.currentThread().getName() + ": ";
    }

    public static void simularActividad(long milisegundos) {
        try {
            Thread.sleep(milisegundos);
        } catch (InterruptedException ex) {
            System.err.println(ex);
        }
    }

    public static void simularActividad(long milisegundos, String actividad) {
        imprimirLineaColor(nombre() + "Empieza a " + actividad);
        simularActividad(milisegundos);
        imprimirLineaColor(nombre() + "Termina de " + actividad);
    }
}
