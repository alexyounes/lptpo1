package lp_tpo1;

import static Utiles.UtilesHilo.simularActividad;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.ArrayList;

/**
 *
 * @author Alejandro Younes
 */
public class Main {

    public static void main(String[] args) {
        final int NConexiones = 10;
        final int NClientes = 5;
        final int NMaxClientes = 3;
        ArrayList<Future<String>> arregloFuture = new ArrayList<>();
        Servidor servidor = new Servidor(NMaxClientes);
        Cliente cliente = new Cliente(servidor);
        ExecutorService executor = Executors.newFixedThreadPool(NClientes);
        for (int i = 0; i < NConexiones; i++) {
            Future<String> future = executor.submit(cliente, "Tarea " + i + " ejecutada");
            simularActividad(500);
            System.out.println(future);
            arregloFuture.add(future);
        }
        executor.shutdown();
        for (int i = 0; i < NConexiones; i++) {
            try {
                System.out.println(arregloFuture.get(i).get());
            } catch (InterruptedException ex) {
                System.out.println("EXCEPCION AL INTERRUMPIR: " + ex);
            } catch (ExecutionException ex) {
                System.out.println("EXCEPCION AL EJECUTAR: " + ex);
            }
        }
    }
}