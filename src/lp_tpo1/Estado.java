package lp_tpo1;

/**
 *
 * @author Alejandro Younes
 */
public abstract class Estado {

    public abstract boolean iniciarConexion(Servidor contexto);

    public abstract boolean finalizarConexion(Servidor contexto);

    void cambiarEstado(Servidor contexto, String estado) {
        contexto.setEstadoActual(estado);
    }
}