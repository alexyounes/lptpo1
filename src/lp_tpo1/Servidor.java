package lp_tpo1;

/**
 *
 * @author Alejandro Younes
 */
public class Servidor {

    private enum Estados {
        Esperando(new Esperando(), "Esperando"),
        SirviendoLibre(new SirviendoLibre(), "SirviendoLibre"),
        SirviendoMaximo(new SirviendoMaximo(), "SirviendoMaximo");
        final Estado estado;
        final String nombre;

        private Estados(Estado estado, String nombre) {
            this.estado = estado;
            this.nombre = nombre;
        }
    }

    private Estado estadoActual;
    private int clientesAct;
    private final int clientesMax;

    Servidor(int clientesMax) {
        System.out.println("Servidor inicia con estado " + Estados.Esperando.nombre);
        estadoActual = Estados.Esperando.estado;
        clientesAct = 0;
        this.clientesMax = clientesMax;
    }

    public synchronized boolean iniciarConexion() {
        return estadoActual.iniciarConexion(this);
    }

    public synchronized boolean finalizarConexion() {
        return estadoActual.finalizarConexion(this);
    }

    public void setEstadoActual(String estadoActual) {
        System.out.println("Servidor cambia a estado " + estadoActual);
        this.estadoActual = Estados.valueOf(estadoActual).estado;
    }

    public void setClientesAct(int clientesAct) {
        this.clientesAct = clientesAct;
    }

    public int getClientesAct() {
        return clientesAct;
    }

    public int getClientesMax() {
        return clientesMax;
    }
}