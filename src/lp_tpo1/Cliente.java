package lp_tpo1;

import static Utiles.UtilesHilo.nombre;
import static Utiles.UtilesHilo.simularActividad;
import static Utiles.UtilesImprimir.imprimirLineaColor;

/**
 *
 * @author Alejandro Younes
 */
public class Cliente implements Runnable {

    public Servidor servidor;

    Cliente(Servidor servidor) {
        this.servidor = servidor;
    }

    public void solicitarArchivo() {
        imprimirLineaColor(nombre() + "Estableciendo conexión...");
        boolean conexionEstablecida = servidor.iniciarConexion();
        if (conexionEstablecida) {
            imprimirLineaColor(nombre() + "Conexión establecida con exito");
            simularActividad(2000, "Descargar archivo");
            imprimirLineaColor(nombre() + "Finalizando conexión...");
            boolean conexionFinalizada = servidor.finalizarConexion();
            if (conexionFinalizada) {
                imprimirLineaColor(nombre() + "Conexión finalizada con exito");
            } else {
                imprimirLineaColor(nombre() + "¡Error! Conexión finalizó anormalmente");
            }
        } else {
            imprimirLineaColor(nombre() + "¡Error! No pudo conectarse al servidor");
        }
    }

    @Override
    public void run() {
        solicitarArchivo();
    }
}
