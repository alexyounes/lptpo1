package lp_tpo1;

/**
 *
 * @author Alejandro Younes
 */
public class Esperando extends Estado {

    public Esperando() {
        System.out.println("ESTADO ESPERANDO");
    }
    
    @Override
    public boolean iniciarConexion(Servidor contexto) {
        int clientesAct = contexto.getClientesAct();
        clientesAct = clientesAct + 1;
        contexto.setClientesAct(clientesAct);
        cambiarEstado(contexto, "SirviendoLibre");
        return true;
    }

    @Override
    public boolean finalizarConexion(Servidor contexto) {
        return false;
    }
}