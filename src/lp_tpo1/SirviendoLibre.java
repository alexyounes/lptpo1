package lp_tpo1;

/**
 *
 * @author Alejandro Younes
 */
public class SirviendoLibre extends Estado {
    
    public SirviendoLibre() {
        System.out.println("ESTADO SIRVIENDOLIBRE");
    }

    @Override
    public boolean iniciarConexion(Servidor contexto) {
        int clientesMax = contexto.getClientesMax();
        int clientesAct = contexto.getClientesAct();
        clientesAct = clientesAct + 1;
        contexto.setClientesAct(clientesAct);
        if (clientesAct == clientesMax) {
            cambiarEstado(contexto, "SirviendoMaximo");
        }
        return true;
    }

    @Override
    public boolean finalizarConexion(Servidor contexto) {
        int clientesAct = contexto.getClientesAct();
        clientesAct = clientesAct - 1;
        contexto.setClientesAct(clientesAct);
        if (clientesAct == 0) {
            cambiarEstado(contexto, "Esperando");
        }
        return true;
    }
}
