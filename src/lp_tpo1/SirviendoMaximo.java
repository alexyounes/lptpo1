package lp_tpo1;

/**
 *
 * @author Alejandro Younes
 */
public class SirviendoMaximo extends Estado {

    public SirviendoMaximo() {
        System.out.println("ESTADO SIRVIENDOMAXIMO");
    }
    
    @Override
    public boolean iniciarConexion(Servidor contexto) {
        return false;
    }

    @Override
    public boolean finalizarConexion(Servidor contexto) {
        int clientesAct = contexto.getClientesAct();
        clientesAct = clientesAct - 1;
        contexto.setClientesAct(clientesAct);
        cambiarEstado(contexto, "SirviendoLibre");
        return true;
    }
}
